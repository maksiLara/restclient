package com.newsapi.larchanka.queringandresponcejson;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

/**
 * Created by Максим on 15.01.2018.
 */

class InternetConnection {

    public static boolean chekConnection(@NonNull Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
}
