package com.newsapi.larchanka.queringandresponcejson.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newsapi.larchanka.queringandresponcejson.R;
import com.newsapi.larchanka.queringandresponcejson.dataNews.Artcile;
import com.newsapi.larchanka.queringandresponcejson.dataNews.Articles;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Максим on 15.01.2018.
 */

public class RecyclerNewsAdapter extends ArrayAdapter<Articles> {

    List<Articles> newsList;
    Context context;
    private LayoutInflater mInflater;

    public RecyclerNewsAdapter(@NonNull Context context, List<Articles> objectList) {
        super(context, 0, objectList);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        newsList = objectList;
    }

    @Nullable
    @Override
    public Articles getItem(int position) {
        return super.getItem(position);
    }

    private static class ViewHolder {
        public final RelativeLayout RELATIVE_LAYOUT;
        @BindView(R.id.imageView)
        public static ImageView imageView;
        @BindView(R.id.textViewOne)
        public static TextView textViewOne;
        @BindView(R.id.textViewSecond)
        public static TextView textViewSecond;

        public ViewHolder(RelativeLayout RELATIVE_LAYOUT, ImageView imageView, TextView textViewOne, TextView textViewSecond) {
            this.RELATIVE_LAYOUT = RELATIVE_LAYOUT;
            this.imageView = imageView;
            this.textViewOne = textViewOne;
            this.textViewSecond = textViewSecond;
        }

        public static ViewHolder create(RelativeLayout RELATIVE_LAYOUT){
            return new ViewHolder(RELATIVE_LAYOUT,imageView,textViewOne,textViewSecond);
        }
    }
}



