package com.newsapi.larchanka.queringandresponcejson.apiClient;

import android.support.annotation.NonNull;
import android.util.Log;

import com.newsapi.larchanka.queringandresponcejson.dataNews.Articles;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Максим on 15.01.2018.
 */

public class RestClient {
    public static final String LOG_REST_CLIENT = "LOG_REST_CLIENT ";
    public static final String BASE_URL = "https://newsapi.org/v2/";
    public static final String API_KEY = "9661e84da22048dbade19553277c3ac7";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static NewsApi getNewsApi() {
        return getRetrofitInstance().create(NewsApi.class);
    }
   /* private final NewsApi newsApiService;*/


  /*  public RestClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        newsApiService = retrofit.create(NewsApi.class);
        Log.d(LOG_REST_CLIENT,"quering");
        Call<Articles> responceArticlesCall = newsApiService.topHeadlines("us", "business");
        try {
            responceArticlesCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(LOG_REST_CLIENT, "");

    }

    OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .addInterceptor(getApiKey()).build();

    @NonNull
    private Interceptor getApiKey() {
        return new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                HttpUrl url = chain.request()
                        .url()
                        .newBuilder()
                        .addQueryParameter("apiKey", API_KEY)
                        .build();
                Request authorizedRequest = chain.request()
                        .newBuilder()
                        .url(url)
                        .build();

                return chain.proceed(authorizedRequest);
            }
        };
    }*/
}
